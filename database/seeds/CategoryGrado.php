<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryGrado extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('grado')->insert([
           [
            'codigo'=>'A1',
            'grado'=>'1º'
           ],
           [
            'codigo'=>'B2',
            'grado'=>'2º'
           ],
           [
            'codigo'=>'C3',
            'grado'=>'3º'
           ],
           [
            'codigo'=>'D4',
            'grado'=>'4º'
           ],
           [
            'codigo'=>'E5',
            'grado'=>'5º'
           ]
        ]);
    }
}
