<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeccion extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('seccion')->insert([
            [
                'codigo'=>'A',
                'seccion'=>'A'
            ],
            [
                'codigo'=>'B',
                'seccion'=>'B'
            ],
            [
                'codigo'=>'C',
                'seccion'=>'C'
            ],
            [
                'codigo'=>'D',
                'seccion'=>'D'
            ],
            [
                'codigo'=>'E',
                'seccion'=>'E'
            ]
        ]);
    }
}
