<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGradoSeccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grado_seccion', function (Blueprint $table) {
            $table->bigIncrements('id');
            //
            $table->unsignedBigInteger('gradoId');
            $table->foreign('gradoId')->references('id')->on('grado');

            $table->unsignedBigInteger('seccionId');
            $table->foreign('seccionId')->references('id')->on('seccion');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grado_seccion');
    }
}
