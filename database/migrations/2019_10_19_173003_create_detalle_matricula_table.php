<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleMatriculaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_matricula', function (Blueprint $table) {
            $table->bigIncrements('id');
            //
            $table->unsignedBigInteger('matriculaId');
            $table->foreign('matriculaId')->references('id')->on('matricula');

            $table->unsignedBigInteger('gradoSeccionId');
            $table->foreign('gradoSeccionId')->references('id')->on('grado_seccion');

            $table->unsignedBigInteger('anioAcademicoId');
            $table->foreign('anioAcademicoId')->references('id')->on('anio_academico');

            $table->unsignedBigInteger('nivelAcademicoId');
            $table->foreign('nivelAcademicoId')->references('id')->on('nivel_academico');


            


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_matricula');
    }
}
